{
    "All Supported Files (*.csv, *.ods, *.xls, *.xlsx)": "Allir studdar skrár  (*.csv, *.ods, *.xls, *.xlsx)", 
    "Microsoft Excel 97-2003 (*.xls)": "Microsoft Excel 97-2003 (*.xls)", 
    "Microsoft Excel 2007-2013 (*.xlsx)": "Microsoft Excel 2007-2013 (*.xlsx)", 
    "OpenDocument-table (*.ods)": "OpenDocument-tafla (*.ods)", 
    "Text CSV (*.csv)": "Texti CSV (*.csv)", 
    "HTML-document (*.html)": "HTML-skjal (*.html)", 
    "Save HTML-document": "Bjarga HTML-skjal", 
    "You have to select a file before you can continue.": "Þú verður að velja skrá áður en þú getur haldið áfram.", 
    "File have successfully been generated.": "Skráin hefur verið búin til.", 
    "Width": "Breidd.", 
    "Used by:": "Notað af:", 
    "HTML Register": "HTML Skrá", 
    "PDF Labels": "PDF Merki", 
    "Font Size": "Fint Stærð", 
    "Font Format": "Fint Snið", 
    "Table Options": "Tafla Valkostir", 
    "Bold": "Feitletrað", 
    "Italic": "Italic", 
    "Underlined": "Undirstrikað", 
    "Text Alignment": "Texta Leiðrétting", 
    "Left": "Vinstri", 
    "Right": "Réttur", 
    "Center": "Miðstöð", 
    "Justify": "Réttlætið", 
    "Searchable": "Leita fær", 
    "Orderable": "Möguleg að raða", 
    "Field Mapping": "Field mapping", 
    "Do not use": "Ekki nota", 
    "Row 1": "Lína 1", 
    "Row 2": "Lína 2", 
    "Row 3": "Lína 3", 
    "Row 4": "Lína 4", 
    "Row 5": "Lína 5", 
    "Row 6": "Lína 6", 
    "Please enter a value": "Vinsamlegast sláðu inn gildi", 
    "Please select a language": "Vinsamlegast veldu tungumál", 
    "Please select output mode": "Vinsamlegast veldu framleiðslustilling", 
    "Finalize": "Lokið - Vista HTML Skrá", 
    "Next": "Næst", 
    "Previous": "Fyrri", 
    "Minimize window": "Lágmarka glugga", 
    "Maximize window": "Hámarka glugga", 
    "Close window": "Lokaðu glugga", 
    "About": "Um", 
    "AS-Xpress is a modular converter application.": "AS-Xpress er einfalda breytirforrit.", 
    "Create registers, labels, etc. easily using spreadsheets created in Excel, OpenOffice, LibreOffice, or other applications that support the following formats.": "Búðu til skrár, merki, osfrv auðveldlega með því að nota töflureikna sem eru búnar til Excel, OpenOffice, LibreOffice, eða önnur forrit sem styðja eftirfarandi snið.", 
    "Built on": "Byggð á", 
    "PDF": "PDF", 
    "HTML": "HTML", 
    "Register": "Skrá", 
    "Labels": "Merki", 
    "Language": "Tungumál", 
    "System Default": "Kerfi Sjálfgefið", 
    "Select file...": "Veldu gagnaskrá...", 
    "Column Settings": "Dálkur Stillingar", 
    "Save settings": "Vista stillingar", 
    "Settings": "Stillingar", 
    "Page Headers": "Síðuhausar", 
    "(optional)": "(valfrjálst)", 
    "Select language": "Veldu tungumál", 
    "Button Text": "Hnappur Texti (Button Text)", 
    "e.g. Open Article": "til dæmis Opna Grein", 
    "Output Mode": "Framleiðsla Háttur", 
    "Select output mode": "Veldu Framleiðsla Háttur", 
    "Development (readable source code)": "Þróun (læsileg frumkóðinn)", 
    "Production (minified source code)": "Framleiðsla (minified frumkóðinn)", 
    "Title...": "Titill ...", 
    "Content...": "Efni ...", 
    "Type": "Gerð", 
    "Create labels...": "Búðu til merki ...", 
    "Please select type": "Vinsamlegast veldu gerð", 
    "Percent (%)": "Hlutfall (%)", 
    "Pixels (px)": "Punktar (px)", 
    "Points (pt)": "Stig (pt)", 
    "Support / Issue Tracker": "Stuðningur / Issue Tracker", 
    "License": "Leyfi", 
    "Wrong value, width can only contain numbers. Enter auto for no width.": "Rangt gildi, breidd getur aðeins innihaldið tölur. Sláðu inn auto sjálfvirkt breidd.", 
    "Wrong value, width cannot be empty.": "Rangt gildi, breidd getur ekki verið tómt. Sláðu inn auto sjálfvirkt breidd.", 
    "Wrong value, font size can only contain numbers.": "Rangt gildi, leturstærð getur aðeins innihaldið tölur.", 
    "Wrong value, font size cannot be empty.": "Rangt gildi, leturstærð getur ekki verið tóm.", 
    "Mapping fields must be unique choices": "Kortlagningarsvið verða að vera einstök val.", 
    "Help us translate": "Hjálpaðu okkur að þýða", 
    "Changelog": ""
}