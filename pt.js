{
    "All Supported Files (*.csv, *.ods, *.xls, *.xlsx)": "Todos os arquivos suportados (*.csv, *.ods, *.xls, *.xlsx)", 
    "Microsoft Excel 97-2003 (*.xls)": "Microsoft Excel 97-2003 (*.xls)", 
    "Microsoft Excel 2007-2013 (*.xlsx)": "Microsoft Excel 2007-2013 (*.xlsx)", 
    "OpenDocument-table (*.ods)": "OpenDocument-tabela (*.ods)", 
    "Text CSV (*.csv)": "Texto CSV (*.csv)", 
    "HTML-document (*.html)": "HTML-documento (*.html)", 
    "Save HTML-document": "Guardar HTML-documento", 
    "You have to select a file before you can continue.": "Você deve selecionar um arquivo antes de continuar.", 
    "File have successfully been generated.": "O arquivo foi gerado com sucesso.", 
    "Width": "Largura", 
    "Used by:": "Usado por:", 
    "HTML Register": "HTML Registro", 
    "PDF Labels": "PDF Etiquetas", 
    "Font Size": "Tamanho da font", 
    "Font Format": "Formato de fonte", 
    "Table Options": "Opções de tabela", 
    "Bold": "Negrito", 
    "Italic": "Itálico", 
    "Underlined": "Sublinhado", 
    "Text Alignment": "Alinhamento de texto", 
    "Left": "Esquerda", 
    "Right": "Certo", 
    "Center": "Centro", 
    "Justify": "Justificado", 
    "Searchable": "Pesquisável", 
    "Orderable": "Possível ordenar", 
    "Field Mapping": "Mapeamento de campo de dados", 
    "Do not use": "Não use", 
    "Row 1": "Linha 1", 
    "Row 2": "Linha 2", 
    "Row 3": "Linha 3", 
    "Row 4": "Linha 4", 
    "Row 5": "Linha 5", 
    "Row 6": "Linha 6", 
    "Please enter a value": "Por favor insira um valor", 
    "Please select a language": "Por favor, selecione um idioma", 
    "Please select output mode": "Por favor, selecione o modo de saída", 
    "Finalize": "Feito - Salve o Registro HTML", 
    "Next": "Seguinte", 
    "Previous": "Anterior", 
    "Minimize window": "Minimizar a janela", 
    "Maximize window": "Maximizar a janela", 
    "Close window": "Janela fechada", 
    "About": "Sobre", 
    "AS-Xpress is a modular converter application.": "AS-Xpress é uma aplicação de conversão modular.", 
    "Create registers, labels, etc. easily using spreadsheets created in Excel, OpenOffice, LibreOffice, or other applications that support the following formats.": "Crie registros, etiquetas, etc. usando facilmente planilhas criadas em Excel, OpenOffice, LibreOffice, ou outros aplicativos que ofereçam suporte aos seguintes formatos.", 
    "Built on": "Construído em", 
    "PDF": "PDF", 
    "HTML": "HTML", 
    "Register": "Registro", 
    "Labels": "Etiquetas", 
    "Language": "Língua", 
    "System Default": "Sistema padrão", 
    "Select file...": "Selecione um arquivo...", 
    "Column Settings": "Configurações de coluna", 
    "Save settings": "Salvar configurações", 
    "Settings": "Configurações", 
    "Page Headers": "Cabeçalho de página", 
    "(optional)": "(opcional)", 
    "Select language": "Selecione o idioma", 
    "Button Text": "Texto do botão (Button Text)", 
    "e.g. Open Article": "v.g. Artigo aberto", 
    "Output Mode": "Modo de saída", 
    "Select output mode": "Selecione o modo de saída", 
    "Development (readable source code)": "Desenvolvimento (código fonte legível)", 
    "Production (minified source code)": "Produção (código fonte minificado)", 
    "Title...": "Título...", 
    "Content...": "Conteúdo...", 
    "Type": "Tipo", 
    "Create labels...": "Criar etiquetas...", 
    "Please select type": "Por favor, Selecione o tipo", 
    "Percent (%)": "Por cento (%)", 
    "Pixels (px)": "Pixels (px)", 
    "Points (pt)": "Pontos (pt)", 
    "Support / Issue Tracker": "Apoio / Issue Tracker", 
    "License": "Licença", 
    "Wrong value, width can only contain numbers. Enter auto for no width.": "O valor errado, a largura só pode conter números.  Digite auto para largura automática.", 
    "Wrong value, width cannot be empty.": "O valor errado, a largura não pode estar vazio.  Digite auto para largura automática.", 
    "Wrong value, font size can only contain numbers.": "O valor incorreto, tamanho da fonte só pode conter números.", 
    "Wrong value, font size cannot be empty.": "O valor incorreto, o tamanho da fonte não pode estar vazio.", 
    "Mapping fields must be unique choices": "Os campos de mapeamento devem ser escolhas exclusivas", 
    "Help us translate": "Ajude-nos a traduzir", 
    "Changelog": ""
}